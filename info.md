Project Name
EthoCaching

Project Tagline/Description (140 Characters Max. Will be used on table card for judging)
Ethereum Geocaching using Graph, Metamask, and Maps.

Team Members First and Last Names:
Matthew Cahn, Emily Shelton

Status.im ID for Each Team Member (we will use this to contact you and your team)
@mpcahn I'm pretty sure.

Detailed Project Description (no more than 3-4 sentences)
An Ethereum Smart Contract that allows users to submit geocache locations to the blockchain. These are tracked using The Graph and are queried from a standard website. The website is used to both view a map of geocaches on the system as well as to submit new ones.

Describe your tech stack (e.g., protocols, languages, API’s, etc.)
The Graph API, Google Maps API, Javascript, jQuery, Bootstrap, Ethereum, Solidity, Node.

Track for which you’re submitting (Open or Impact)
Open

All Bounties Completed/Incorporated
The Graph - Bounty 1: Best usage of The Graph The Graph - Bounty 2: Best new subgraph on Graph Explorer