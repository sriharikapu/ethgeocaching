import { AddCache } from './types/Geocaching/Geocaching'
import { Geocache } from './types/schema'

export function handleAddCache(event: AddCache): void {
  let cache = new Geocache(event.transaction.hash.toHex())
  cache.address = event.params.sender
  cache.title = event.params.title
  cache.description = event.params.description
  cache.lat = event.params.lat
  cache.long = event.params.long
  cache.save()
}

