import {
  TypedMap,
  Entity,
  Value,
  ValueKind,
  store,
  Address,
  Bytes,
  BigInt
} from "@graphprotocol/graph-ts";

export class Geocache extends Entity {
  constructor(id: string) {
    this.entries = new Array(0);
    this.set("id", Value.fromString(id));
  }

  save(): void {
    let id = this.get("id");
    assert(id !== null, "Cannot save Geocache entity without an ID");
    assert(
      id.kind == ValueKind.STRING,
      "Cannot save Geocache entity with non-string ID. " +
        'Considering using .toHex() to convert the "id" to a string.'
    );
    store.set("Geocache", id.toString(), this);
  }

  static load(id: string): Geocache | null {
    return store.get("Geocache", id) as Geocache | null;
  }

  get id(): string {
    let value = this.get("id");
    return value.toString();
  }

  set id(value: string) {
    this.set("id", Value.fromString(value));
  }

  get address(): Bytes {
    let value = this.get("address");
    return value.toBytes();
  }

  set address(value: Bytes) {
    this.set("address", Value.fromBytes(value));
  }

  get title(): string {
    let value = this.get("title");
    return value.toString();
  }

  set title(value: string) {
    this.set("title", Value.fromString(value));
  }

  get description(): string {
    let value = this.get("description");
    return value.toString();
  }

  set description(value: string) {
    this.set("description", Value.fromString(value));
  }

  get lat(): BigInt {
    let value = this.get("lat");
    return value.toBigInt();
  }

  set lat(value: BigInt) {
    this.set("lat", Value.fromBigInt(value));
  }

  get long(): BigInt {
    let value = this.get("long");
    return value.toBigInt();
  }

  set long(value: BigInt) {
    this.set("long", Value.fromBigInt(value));
  }
}
