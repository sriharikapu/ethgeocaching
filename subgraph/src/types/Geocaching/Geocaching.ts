import {
  EthereumEvent,
  SmartContract,
  EthereumValue,
  JSONValue,
  TypedMap,
  Entity,
  Bytes,
  Address,
  BigInt
} from "@graphprotocol/graph-ts";

export class AddCache extends EthereumEvent {
  get params(): AddCacheParams {
    return new AddCacheParams(this);
  }
}

export class AddCacheParams {
  _event: AddCache;

  constructor(event: AddCache) {
    this._event = event;
  }

  get sender(): Address {
    return this._event.parameters[0].value.toAddress();
  }

  get title(): string {
    return this._event.parameters[1].value.toString();
  }

  get description(): string {
    return this._event.parameters[2].value.toString();
  }

  get lat(): BigInt {
    return this._event.parameters[3].value.toBigInt();
  }

  get long(): BigInt {
    return this._event.parameters[4].value.toBigInt();
  }
}

export class Geocaching extends SmartContract {
  static bind(address: Address): Geocaching {
    return new Geocaching("Geocaching", address);
  }
}
