# Ethereum GeoCaching

Adds Geocaching data to a smart contract on the Ethereum Rinkaby Test network and renders that data to a google map.

![Screenshot](img/ethGeoCaching.jpg)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

Clone the repository, cd into subgraph directory and type:

```
npm install
```
to install dependencies for the subgraph.

To interact with the site login to Metamask and change the network to the Rinkaby Test Net. Click anywhere on the map to render the geocaches that are stored on the blockchain. Click on a position on the map to record the latitude and longitude of the map location, add a title and description and press submit. Then confirm the transaction on metamask. Refresh the page in a minute or two and the new geocache location should be rendered to the map.

### Prerequisites

What things you need to install the software and how to install them

Metamask Chrome extension with ETH on the Rinkaby Test Network

## Built With

* [TheGraph](https://thegraph.com/) - Protocol for indexing and querying data from Ethereum.
* [Remix](http://remix.ethereum.org/) - Solidity IDE
* [Google Maps API](https://developers.google.com/maps/documentation/) - Google Maps Platform
* [Metamask](https://metamask.io/) - Metamask Chrome Extension

## Authors

* **Matthew Cahn** - *Initial work* - [mpcahn.co](https://mpcahn.co)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Jannis Pohlmann, Nena Djaja, and the rest of [The Graph](https://thegraph.com/) Team. 