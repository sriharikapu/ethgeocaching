pragma solidity ^0.5.00;

// QuadKeys for Lat Long
//  XYO Github
// a simple set and get function for geocache defined: 
contract geocaching{
    event AddCache(address sender, string title, string description, uint64 lat, uint64 long);

    
    string title;
    string description;
    uint64 lat;
    uint64 long;
           
      
    function addCache(string memory _title, string memory _description, uint64 _lat, uint64 _long) public {
        title = _title;
        lat = _lat;
        description = _description;
        long = _long;

        emit AddCache(msg.sender, _title, _description, _lat, _long);        
    }     
}